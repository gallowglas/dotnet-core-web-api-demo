# DotNet Core Web API Demo

This is a really simple example of a RESTful DotNet Core Web API. It makes use of a factory pattern, dependency injection (DI), and leverages AutoMapper, StructureMap, and Swagger.

You can access the swagger UI by replacing "/api/animal/etc" with "/swagger".