﻿namespace WebAPIDemo.Models
{
    public class Dog : Animal, IAnimal // ConcreteProduct
    {
        public Dog(string id)
        {
            Id = id;
            Type = "dog";
        }

        public override string Speak()
        {
            return $"{Id} says BARK!";
        }
    }
}