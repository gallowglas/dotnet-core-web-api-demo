﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace WebAPIDemo.Models
{
    public interface IAnimal
    {
        string Id { get; set; }
        string Type { get; set; }
        string Speak();
    }
}
